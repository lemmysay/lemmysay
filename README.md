# LemmySay

LemmySay (or lemmy-say, or lemmy say or lemmysay) is an android application allowing to brows
Lemmy communities (see: https://github.com/LemmyNet/lemmy)


## Contributing

### Prerequisites
* Java JDK 14
* Android SDK 29
* Kotlin 1.3.72

### About formatting
Before pushing any merge request, ensure your code is properly formatted regarding the rules
located in the `.editorconfig` file. 
You can enforce application of those rules by running `./gradlew kotlinFormat` before committing your code. 

There are indeed some linting rules that would prevent any code that doesn't comply to those rules to be automatically 
rejected by the continuous integration (CI).
