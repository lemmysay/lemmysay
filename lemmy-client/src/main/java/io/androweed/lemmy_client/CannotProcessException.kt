package io.androweed.lemmy_client

class CannotProcessException(message: String) : Exception(message)