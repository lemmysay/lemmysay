package io.androweed.lemmy_client

import java.time.LocalDateTime

class Community(
    val id: Int,
    val name: String,
    val title: String,
    val description: String?,
    val categoryId: Int,
    val creatorId: Int,
    val removed: Boolean,
    val published: LocalDateTime,
    val updated: LocalDateTime?,
    val deleted: Boolean,
    val nsfw: Boolean,
    val creatorName: String,
    val creatorAvatar: String?,
    val categoryName: String,
    val numberOfSubscribers: Int,
    val numberOfPosts: Int,
    val numberOfComments: Int,
    val hotRank: Int,
    val userId: Int?,
    val subscribed: Boolean?
)