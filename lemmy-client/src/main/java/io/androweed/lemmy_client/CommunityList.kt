package io.androweed.lemmy_client

data class CommunityList(
    val communities: List<Community> = emptyList()
)