package io.androweed.lemmy_client

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.isSuccessful
import com.github.kittinunf.fuel.gson.responseObject
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import java.lang.reflect.Type
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class LemmyClient(
    url: String
) {

    /**
     * rootUrl is the specified url with its trailing '/' removed if any
     */
    private val rootUrl = url.removeSuffix("/")

    private val gson by lazy {
        GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .registerTypeAdapter(LocalDateTime::class.java, LocalDateTimeConverter)
            .create()
    }

    /**
     * Returns a list of accessible communities
     *
     * @param sort How the results should be sorted
     *
     * @see Sort
     */
    fun communities(sort: Sort = Sort.HOT): CommunityList? {
        val response = Fuel
            .get("$rootUrl/api/v1/communities/list", listOf("sort" to sort.queryParameterValue))
            .responseObject<CommunityList>(gson)

        if (!response.second.isSuccessful) {
            throw CannotProcessException(
                "received error code ${response.second.statusCode} with message ${response.second.responseMessage}")
        }

        return response.third.component1()
    }

    enum class Sort(val queryParameterValue: String) {

        /**
         * the hottest posts/communities, depending on votes, views, comments and publish date
         */
        HOT("Hot"),

        /**
         * the newest posts/communities
         */
        NEW("New"),

        /**
         * the most upvoted posts/communities of the current day.
         */
        TOP_DAY("TopDay"),

        /**
         * he most upvoted posts/communities of the current week.
         */
        TOP_WEEK("TopWeek"),

        /**
         * the most upvoted posts/communities of the current month.
         */
        TOP_MONTH("TopMonth"),

        /**
         * the most upvoted posts/communities of the current year.
         */
        TOP_YEAR("TopYear"),

        /**
         * the most upvoted posts/communities on the current instance.
         */
        TOP_ALL("TopAll")
    }

    /**
     * Custom [LocalDateTime] serializer/deserializer... as this is not supported by default by Gson lib
     */
    private object LocalDateTimeConverter : JsonSerializer<LocalDateTime>, JsonDeserializer<LocalDateTime> {
        override fun serialize(src: LocalDateTime?, typeOfSrc: Type?, context: JsonSerializationContext?) =
            JsonPrimitive(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(src))

        override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?) =
            LocalDateTime.parse(json?.asString, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
    }
}