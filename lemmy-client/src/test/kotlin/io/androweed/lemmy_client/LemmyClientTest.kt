package io.androweed.lemmy_client

import assertk.assertAll
import assertk.assertThat
import assertk.assertions.containsExactly
import assertk.assertions.extracting
import assertk.assertions.hasClass
import assertk.assertions.hasSize
import assertk.assertions.isEmpty
import assertk.assertions.isEqualTo
import assertk.assertions.isFailure
import java.time.LocalDateTime
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class LemmyClientTest {

    private lateinit var lemmyClient: LemmyClient

    private var mockWebServer = MockWebServer()

    @BeforeEach
    fun prepareTest() {
        // The url where the request should be executed should be the one exposed by mockWebServer
        mockWebServer.start()

        lemmyClient = LemmyClient(mockWebServer.url("/").toString())
    }

    @AfterEach
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Nested
    inner class ListCommunities {

        @Test
        fun `it should perform request on the right path when requesting for communities`() {
            // Given
            mockWebServer.enqueue(MockResponse().setResponseCode(200))

            // When
            lemmyClient.communities()

            // Then
            assertThat(mockWebServer.requestCount).isEqualTo(1)

            val request = mockWebServer.takeRequest()
            assertAll {
                assertThat(request.method).isEqualTo("GET")
                assertThat(request.requestUrl?.encodedPath).isEqualTo("/api/v1/communities/list")
            }
        }

        @Test
        fun `it should perform request with the default hot sorting parameter if none specified`() {
            // Given
            mockWebServer.enqueue(MockResponse().setResponseCode(200))

            // When
            lemmyClient.communities()

            // Then
            assertThat(mockWebServer.requestCount).isEqualTo(1)

            val request = mockWebServer.takeRequest()
            assertThat(request.requestUrl?.queryParameter("sort")).isEqualTo("Hot")
        }

        @Test
        fun `it should perform request with the HOT sorting parameter`() {
            // Given
            mockWebServer.enqueue(MockResponse().setResponseCode(200))

            // When
            lemmyClient.communities(LemmyClient.Sort.HOT)

            // Then
            assertThat(mockWebServer.requestCount).isEqualTo(1)

            val request = mockWebServer.takeRequest()
            assertThat(request.requestUrl?.queryParameter("sort")).isEqualTo("Hot")
        }

        @Test
        fun `it should perform request with the NEW sorting parameter`() {
            // Given
            mockWebServer.enqueue(MockResponse().setResponseCode(200))

            // When
            lemmyClient.communities(LemmyClient.Sort.NEW)

            // Then
            assertThat(mockWebServer.requestCount).isEqualTo(1)

            val request = mockWebServer.takeRequest()
            assertThat(request.requestUrl?.queryParameter("sort")).isEqualTo("New")
        }

        @Test
        fun `it should perform request with the TOP_DAY sorting parameter`() {
            // Given
            mockWebServer.enqueue(MockResponse().setResponseCode(200))

            // When
            lemmyClient.communities(LemmyClient.Sort.TOP_DAY)

            // Then
            assertThat(mockWebServer.requestCount).isEqualTo(1)

            val request = mockWebServer.takeRequest()
            assertThat(request.requestUrl?.queryParameter("sort")).isEqualTo("TopDay")
        }

        @Test
        fun `it should perform request with the TOP_WEEK sorting parameter`() {
            // Given
            mockWebServer.enqueue(MockResponse().setResponseCode(200))

            // When
            lemmyClient.communities(LemmyClient.Sort.TOP_WEEK)

            // Then
            assertThat(mockWebServer.requestCount).isEqualTo(1)

            val request = mockWebServer.takeRequest()
            assertThat(request.requestUrl?.queryParameter("sort")).isEqualTo("TopWeek")
        }

        @Test
        fun `it should perform request with the TOP_MONTH sorting parameter`() {
            // Given
            mockWebServer.enqueue(MockResponse().setResponseCode(200))

            // When
            lemmyClient.communities(LemmyClient.Sort.TOP_MONTH)

            // Then
            assertThat(mockWebServer.requestCount).isEqualTo(1)

            val request = mockWebServer.takeRequest()
            assertThat(request.requestUrl?.queryParameter("sort")).isEqualTo("TopMonth")
        }

        @Test
        fun `it should perform request with the TOP_YEAR sorting parameter`() {
            // Given
            mockWebServer.enqueue(MockResponse().setResponseCode(200))

            // When
            lemmyClient.communities(LemmyClient.Sort.TOP_YEAR)

            // Then
            assertThat(mockWebServer.requestCount).isEqualTo(1)

            val request = mockWebServer.takeRequest()
            assertThat(request.requestUrl?.queryParameter("sort")).isEqualTo("TopYear")
        }

        @Test
        fun `it should perform request with the TOP_ALL sorting parameter`() {
            // Given
            mockWebServer.enqueue(MockResponse().setResponseCode(200))

            // When
            lemmyClient.communities(LemmyClient.Sort.TOP_ALL)

            // Then
            assertThat(mockWebServer.requestCount).isEqualTo(1)

            val request = mockWebServer.takeRequest()
            assertThat(request.requestUrl?.queryParameter("sort")).isEqualTo("TopAll")
        }

        @Test
        fun `it should return no communities if none are accessible`() {

            // Given
            mockWebServer.enqueue(MockResponse()
                .setResponseCode(200)
                .setBody("""
                    { "communities": [] }
                    """))
            // When
            val communities = lemmyClient.communities()

            // Then
            assertThat(communities?.communities!!).isEmpty()
        }

        @Test
        fun `it should raise exception if query doesn't return code HTTP 200`() {
            // Given
            mockWebServer.enqueue(MockResponse()
                .setResponseCode(400))

            // When
            assertThat {
                lemmyClient.communities()
            }.isFailure().hasClass(CannotProcessException::class)
        }

        @Test
        fun `it should return every available communities`() {
            // Given
            mockWebServer.enqueue(MockResponse()
                .setResponseCode(200)
                .setBody("""
                    {
                        "communities": [
                        {
                          "id": 15331,
                          "name": "amethyst",
                          "title": "Amethyst game engine unoffical Lemmy community",
                          "description": "Amethyst is a game engine written in Rust. And this is his unoffical community.",
                          "category_id": 21,
                          "creator_id": 9656,
                          "removed": false,
                          "published": "2020-06-12T06:56:27.989107",
                          "updated": null,
                          "deleted": false,
                          "nsfw": false,
                          "creator_name": "Voodlaz",
                          "creator_avatar": "https://dev.lemmy.ml/pictshare/ek4a20.jpg",
                          "category_name": "Programming/Software",
                          "number_of_subscribers": 2,
                          "number_of_posts": 0,
                          "number_of_comments": 0,
                          "hot_rank": 72,
                          "user_id": null,
                          "subscribed": null
                        },
                        {
                          "id": 15328,
                          "name": "nicheinternet",
                          "title": "Weird Corners of the Internet",
                          "description": "SFW weird corners of the internet. See something amusingly specific? Could belong here.",
                          "category_id": 2,
                          "creator_id": 8937,
                          "removed": false,
                          "published": "2020-06-11T18:14:18.256383",
                          "updated": null,
                          "deleted": false,
                          "nsfw": false,
                          "creator_name": "kixiQu",
                          "creator_avatar": "https://dev.lemmy.ml/pictshare/pcq935.gif",
                          "category_name": "Humor/Memes",
                          "number_of_subscribers": 6,
                          "number_of_posts": 2,
                          "number_of_comments": 1,
                          "hot_rank": 28,
                          "user_id": null,
                          "subscribed": null
                        }]
                    }
                        """))

            // When
            val communities = lemmyClient.communities()

            // Then
            assertAll {
                assertThat(communities?.communities!!).hasSize(2)
                assertThat(communities.communities).extracting(Community::id).containsExactly(15331, 15328)
                assertThat(communities.communities).extracting(Community::name)
                    .containsExactly("amethyst", "nicheinternet")
                assertThat(communities.communities).extracting(Community::title)
                    .containsExactly("Amethyst game engine unoffical Lemmy community", "Weird Corners of the Internet")
                assertThat(communities.communities).extracting(Community::description)
                    .containsExactly(
                        "Amethyst is a game engine written in Rust. And this is his unoffical community.",
                        "SFW weird corners of the internet. See something amusingly specific? Could belong here.")
                assertThat(communities.communities).extracting(Community::categoryId).containsExactly(21, 2)
                assertThat(communities.communities).extracting(Community::creatorId).containsExactly(9656, 8937)
                assertThat(communities.communities).extracting(Community::removed).containsExactly(false, false)
                assertThat(communities.communities).extracting(Community::published)
                    .containsExactly(
                        LocalDateTime.parse("2020-06-12T06:56:27.989107"),
                        LocalDateTime.parse("2020-06-11T18:14:18.256383"))
                assertThat(communities.communities).extracting(Community::updated).containsExactly(null, null)
                assertThat(communities.communities).extracting(Community::deleted).containsExactly(false, false)
                assertThat(communities.communities).extracting(Community::nsfw).containsExactly(false, false)
                assertThat(communities.communities).extracting(Community::creatorName)
                    .containsExactly("Voodlaz", "kixiQu")
                assertThat(communities.communities).extracting(Community::creatorAvatar)
                    .containsExactly(
                        "https://dev.lemmy.ml/pictshare/ek4a20.jpg",
                        "https://dev.lemmy.ml/pictshare/pcq935.gif")
                assertThat(communities.communities).extracting(Community::categoryName)
                    .containsExactly("Programming/Software", "Humor/Memes")
                assertThat(communities.communities).extracting(Community::numberOfSubscribers).containsExactly(2, 6)
                assertThat(communities.communities).extracting(Community::numberOfPosts).containsExactly(0, 2)
                assertThat(communities.communities).extracting(Community::numberOfComments).containsExactly(0, 1)
                assertThat(communities.communities).extracting(Community::hotRank).containsExactly(72, 28)
                assertThat(communities.communities).extracting(Community::userId).containsExactly(null, null)
                assertThat(communities.communities).extracting(Community::subscribed).containsExactly(null, null)
            }
        }
    }
}